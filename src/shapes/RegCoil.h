#ifndef REGCOIL_H
#define REGCOIL_H

#include "wx/wxsf/wxShapeFramework.h"

class cRegCoil : public wxSFCircleShape
{
public:
    XS_DECLARE_CLONABLE_CLASS(cRegCoil);

    cRegCoil();
    cRegCoil(const cRegCoil& obj);
    virtual ~cRegCoil();

    // public virtual functions
    virtual bool OnKey(int key);

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

#endif // REGCOIL_H

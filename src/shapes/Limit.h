#ifndef LIMIT_H
#define LIMIT_H

#include "wx/wxsf/wxShapeFramework.h"

class cLimit : public wxSFPolygonShape
{
public:
    XS_DECLARE_CLONABLE_CLASS(cLimit);

    cLimit();
    cLimit(const cLimit& obj);
    virtual ~cLimit();

    // public virtual functions
    virtual bool OnKey(int key);

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

#endif // LIMIT_H

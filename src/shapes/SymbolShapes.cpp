#include "../gui/wx_pch.h"
#include "SymbolShape.h"

// implement RTTI information and xsSerializable::Clone() functions
XS_IMPLEMENT_CLONABLE_CLASS(cRegContactShape, wxSFPolygonShape);

// define regcontact shape
const wxRealPoint regcontact[16]={wxRealPoint(40,-30), wxRealPoint(40,30), wxRealPoint(40,0), wxRealPoint(0,0),
                            wxRealPoint(0,50), wxRealPoint(100,50), wxRealPoint(100,0), wxRealPoint(60,0),
                            wxRealPoint(60,30), wxRealPoint(60,-30), wxRealPoint(60,0), wxRealPoint(100,0),
                            wxRealPoint(100,-50), wxRealPoint(0,-50), wxRealPoint(0,0), wxRealPoint(40,0)};

cRegContactShape::cRegContactShape()
{
    // disable serialization of polygon vertices, because they are always set
    // in this constructor
	EnablePropertySerialization(wxT("vertices"), false);
    // set polygon vertices
	SetVertices(16, regcontact);

    // initialize shape
    Initialize();
}

cRegContactShape::cRegContactShape(const wxRealPoint& pos, wxSFDiagramManager* manager)
: wxSFPolygonShape(16, regcontact, pos, manager)
{
    // disable serialization of polygon vertices, because they are always set
    // in constructor
	EnablePropertySerialization(wxT("vertices"), false);

    // initialize shape
    Initialize();
}

cRegContactShape::cRegContactShape(const cRegContactShape& obj)
: wxSFPolygonShape(obj)
{
	// clone source child text object..
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
	if( m_pText )
	{
		// .. and append it to this shapes as its child
		AddChild(m_pText);
		// this object is created by the parent class constructor and not
		// by the serializer (only its properties are deserialized)
		XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
	}
}

cRegContactShape::~cRegContactShape()
{
}

void cRegContactShape::Initialize()
{
    // initialize custom data members...
    m_sDescription = wxT("Regular Contact");
    // now tell the serializer that this data member should be serialized
    // (see library documentation to get more info about available serialization
    // macros (supported data types))
    XS_SERIALIZE(m_sDescription, wxT("description"));

    // polygon-based shapes can be connected either to the vertices or to the
    // nearest border point (default value is TRUE).
    SetConnectToVertex(false);

    // set accepted connections for the new shape
    AcceptConnection(wxT("All"));
//    AcceptSrcNeighbour(wxT("cRegContactShape"));
//    AcceptTrgNeighbour(wxT("cRegContactShape"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    // shapes can have fixed connection points defined in the following way:
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
	AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

	// create associated shape(s)
	m_pText = new wxSFEditTextShape();
    // set some properties
    if(m_pText)
    {
        // set text
        m_pText->SetText(wxT("label"));

        // set alignment
        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

        // set required shape style(s)
		m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);
		// you can also force displaying of the shapes handles even if the interactive
		// size change is not allowed:
		//m_pText->AddStyle(sfsSHOW_HANDLES);

        // components of composite shapes created at runtime in parent shape's
        // constructor cannot be fully serialized (it means created by
		// the serializer) so it is important to disable their standard serialization
        // but they can be still serialized as the parent shape's properties
		// in the standard way by the following macro:
		SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}



// implement RTTI information and xsSerializable::Clone() functions
XS_IMPLEMENT_CLONABLE_CLASS(cNotContactShape, wxSFPolygonShape);

// define regcontact shape
const wxRealPoint notcontact[25]={wxRealPoint(0,0), wxRealPoint(40,0), wxRealPoint(40,-30), wxRealPoint(40,30),
                            wxRealPoint(60,-30), wxRealPoint(60,30), wxRealPoint(60,0), wxRealPoint(100,0),
                            wxRealPoint(100,50), wxRealPoint(0,50), wxRealPoint(0,-50), wxRealPoint(100,-50),
                            wxRealPoint(100,0), wxRealPoint(100,-50), wxRealPoint(0,-50), wxRealPoint(0,0),
                            wxRealPoint(40,0), wxRealPoint(40,-30), wxRealPoint(40,30), wxRealPoint(60,-30),
                            wxRealPoint(60,30), wxRealPoint(60,0), wxRealPoint(100,0), wxRealPoint(100,-50),
                            wxRealPoint(0,-50)};

cNotContactShape::cNotContactShape()
{
    // disable serialization of polygon vertices, because they are always set
    // in this constructor
    EnablePropertySerialization(wxT("vertices"), false);
    // set polygon vertices
    SetVertices(25, notcontact);

    // initialize shape
    Initialize();
}

cNotContactShape::cNotContactShape(const wxRealPoint& pos, wxSFDiagramManager* manager)
: wxSFPolygonShape(25, notcontact, pos, manager)
{
    // disable serialization of polygon vertices, because they are always set
    // in constructor
    EnablePropertySerialization(wxT("vertices"), false);

    // initialize shape
    Initialize();
}

cNotContactShape::cNotContactShape(const cNotContactShape& obj)
: wxSFPolygonShape(obj)
{
    // clone source child text object..
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
    if( m_pText )
    {
        // .. and append it to this shapes as its child
        AddChild(m_pText);
        // this object is created by the parent class constructor and not
        // by the serializer (only its properties are deserialized)
        XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
    }
}

cNotContactShape::~cNotContactShape()
{
}

void cNotContactShape::Initialize()
{
    // initialize custom data members...
    m_sDescription = wxT("Not Contact");
    // now tell the serializer that this data member should be serialized
    // (see library documentation to get more info about available serialization
    // macros (supported data types))
    XS_SERIALIZE(m_sDescription, wxT("description"));

    // polygon-based shapes can be connected either to the vertices or to the
    // nearest border point (default value is TRUE).
    SetConnectToVertex(false);

    // set accepted connections for the new shape
    AcceptConnection(wxT("All"));
//    AcceptSrcNeighbour(wxT("cRegContactShape"));
//    AcceptTrgNeighbour(wxT("cRegContactShape"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    // shapes can have fixed connection points defined in the following way:
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

    // create associated shape(s)
    m_pText = new wxSFEditTextShape();
    // set some properties
    if(m_pText)
    {
        // set text
        m_pText->SetText(wxT("label"));

        // set alignment
        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

        // set required shape style(s)
        m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);
        // you can also force displaying of the shapes handles even if the interactive
        // size change is not allowed:
        //m_pText->AddStyle(sfsSHOW_HANDLES);

        // components of composite shapes created at runtime in parent shape's
        // constructor cannot be fully serialized (it means created by
        // the serializer) so it is important to disable their standard serialization
        // but they can be still serialized as the parent shape's properties
        // in the standard way by the following macro:
        SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}




// implement RTTI information and xsSerializable::Clone() functions
XS_IMPLEMENT_CLONABLE_CLASS(cLimitShape, wxSFPolygonShape);

// define limit shape
const wxRealPoint limit[4]={wxRealPoint(0,-50), wxRealPoint(10,-50), wxRealPoint(10,50), wxRealPoint(0,50)};

cLimitShape::cLimitShape()
{
    // disable serialization of polygon vertices, because they are always set
    // in this constructor
    EnablePropertySerialization(wxT("vertices"), false);
    // set polygon vertices
    SetVertices(4, limit);

    // initialize shape
    Initialize();
}

cLimitShape::cLimitShape(const wxRealPoint& pos, wxSFDiagramManager* manager)
: wxSFPolygonShape(4, limit, pos, manager)
{
    // disable serialization of polygon vertices, because they are always set
    // in constructor
    EnablePropertySerialization(wxT("vertices"), false);

    // initialize shape
    Initialize();
}

cLimitShape::cLimitShape(const cLimitShape& obj)
: wxSFPolygonShape(obj)
{
    // clone source child text object..
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
    if( m_pText )
    {
        // .. and append it to this shapes as its child
        AddChild(m_pText);
        // this object is created by the parent class constructor and not
        // by the serializer (only its properties are deserialized)
        XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
    }
}

cLimitShape::~cLimitShape()
{
}

void cLimitShape::Initialize()
{
    // initialize custom data members...
    m_sDescription = wxT("Limits");
    // now tell the serializer that this data member should be serialized
    // (see library documentation to get more info about available serialization
    // macros (supported data types))
    XS_SERIALIZE(m_sDescription, wxT("description"));

    // polygon-based shapes can be connected either to the vertices or to the
    // nearest border point (default value is TRUE).
    SetConnectToVertex(false);

    SetFill(wxBrush(*wxBLACK));

    // set accepted connections for the new shape
    AcceptConnection(wxT("All"));
//    AcceptSrcNeighbour(wxT("cLimitShape"));
//    AcceptTrgNeighbour(wxT("cLimitShape"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    // shapes can have fixed connection points defined in the following way:
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

    // create associated shape(s)
    m_pText = new wxSFEditTextShape();
    // set some properties
    if(m_pText)
    {
        // set text
        m_pText->SetText(wxT("0"));

        // set alignment
        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

        // set required shape style(s)
        m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);
        // you can also force displaying of the shapes handles even if the interactive
        // size change is not allowed:
        //m_pText->AddStyle(sfsSHOW_HANDLES);

        // components of composite shapes created at runtime in parent shape's
        // constructor cannot be fully serialized (it means created by
        // the serializer) so it is important to disable their standard serialization
        // but they can be still serialized as the parent shape's properties
        // in the standard way by the following macro:
        SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}

#ifndef NOTCONTACT_H
#define NOTCONTACT_H

#include "wx/wxsf/wxShapeFramework.h"

class cNotContact : public wxSFPolygonShape
{
public:
    XS_DECLARE_CLONABLE_CLASS(cNotContact);

    cNotContact();
    cNotContact(const cNotContact& obj);
    virtual ~cNotContact();

    // public virtual functions
    virtual bool OnKey(int key);

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

#endif // NOTCONTACT_H

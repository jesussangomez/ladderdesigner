#ifndef SYMBOLSHAPE_H
#define SYMBOLSHAPE_H

// include main wxSF header file
#include "wx/wxsf/wxShapeFramework.h"

class cRegContactShape : public wxSFPolygonShape
{
public:
    // enable RTTI information and define xsSerializable::Clone() function used by
	// wxXmlSerilizer::CopyItems() function, the data (shape) manager's
	// copy constructor and in some cases also by the canvas history manager.
    XS_DECLARE_CLONABLE_CLASS(cRegContactShape);

    // default constructor used by RTTI
    cRegContactShape();
    // user constructor
    cRegContactShape(const wxRealPoint& pos, wxSFDiagramManager* manager);
    // copy constructor needed by the xsSerializable::Clone() function
    cRegContactShape(const cRegContactShape& obj);
    // destructor
    virtual ~cRegContactShape();

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

class cNotContactShape : public wxSFPolygonShape
{
public:
    // enable RTTI information and define xsSerializable::Clone() function used by
    // wxXmlSerilizer::CopyItems() function, the data (shape) manager's
    // copy constructor and in some cases also by the canvas history manager.
    XS_DECLARE_CLONABLE_CLASS(cNotContactShape);

    // default constructor used by RTTI
    cNotContactShape();
    // user constructor
    cNotContactShape(const wxRealPoint& pos, wxSFDiagramManager* manager);
    // copy constructor needed by the xsSerializable::Clone() function
    cNotContactShape(const cNotContactShape& obj);
    // destructor
    virtual ~cNotContactShape();

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

class cLimitShape : public wxSFPolygonShape
{
public:
    // enable RTTI information and define xsSerializable::Clone() function used by
    // wxXmlSerilizer::CopyItems() function, the data (shape) manager's
    // copy constructor and in some cases also by the canvas history manager.
    XS_DECLARE_CLONABLE_CLASS(cLimitShape);

    // default constructor used by RTTI
    cLimitShape();
    // user constructor
    cLimitShape(const wxRealPoint& pos, wxSFDiagramManager* manager);
    // copy constructor needed by the xsSerializable::Clone() function
    cLimitShape(const cLimitShape& obj);
    // destructor
    virtual ~cLimitShape();

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

#endif // SYMBOLSHAPE_H

#include "../gui/wx_pch.h"
#include "Limit.h"

XS_IMPLEMENT_CLONABLE_CLASS(cLimit, wxSFPolygonShape);

const wxRealPoint limit[4]={wxRealPoint(0,-50), wxRealPoint(20,-50), wxRealPoint(20,50), wxRealPoint(0,50)};

cLimit::cLimit()
{
    EnablePropertySerialization(wxT("vertices"), false);
    SetVertices(4, limit);

    Initialize();
}

cLimit::cLimit(const cLimit& obj) : wxSFPolygonShape(obj)
{
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
    if( m_pText )
    {
        AddChild(m_pText);
        XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
    }
}

cLimit::~cLimit()
{
}

void cLimit::Initialize()
{
    m_sDescription = wxT("Limits");
    XS_SERIALIZE(m_sDescription, wxT("description"));
    SetConnectToVertex(false);

    SetFill(wxBrush(*wxGREY_BRUSH));

    AcceptConnection(wxT("All"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
    AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

    m_pText = new wxSFEditTextShape();
    if(m_pText)
    {
        m_pText->SetText(wxT("0"));

        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

        m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);

        SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}

bool cLimit::OnKey(int key)
{
    switch(key)
    {
        case WXK_F2:
            if(m_pText->IsActive() && m_pText->IsVisible())
            {
                m_pText->EditLabel();
            }
            break;

        default:
            break;
    }

    return true;
}

#ifndef REGCONTACT_H
#define REGCONTACT_H

#include "wx/wxsf/wxShapeFramework.h"

class cRegContact : public wxSFPolygonShape
{
public:
    XS_DECLARE_CLONABLE_CLASS(cRegContact);

    cRegContact();
    cRegContact(const cRegContact& obj);
    virtual ~cRegContact();

    // public virtual functions
    virtual bool OnKey(int key);

protected:
    // protected data members
    wxString m_sDescription;
    wxSFEditTextShape* m_pText;

    // protected functions;
    void Initialize();
};

#endif // REGCONTACT_H

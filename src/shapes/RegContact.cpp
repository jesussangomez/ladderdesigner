#include "../gui/wx_pch.h"
#include "RegContact.h"

XS_IMPLEMENT_CLONABLE_CLASS(cRegContact, wxSFPolygonShape);

const wxRealPoint regcontact[16]={wxRealPoint(40,-30), wxRealPoint(40,30), wxRealPoint(40,0), wxRealPoint(0,0),
                            wxRealPoint(0,50), wxRealPoint(100,50), wxRealPoint(100,0), wxRealPoint(60,0),
                            wxRealPoint(60,30), wxRealPoint(60,-30), wxRealPoint(60,0), wxRealPoint(100,0),
                            wxRealPoint(100,-50), wxRealPoint(0,-50), wxRealPoint(0,0), wxRealPoint(40,0)};

cRegContact::cRegContact()
{
	EnablePropertySerialization(wxT("vertices"), false);
	SetVertices(16, regcontact);

    Initialize();
}

cRegContact::cRegContact(const cRegContact& obj) : wxSFPolygonShape(obj)
{
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
	if( m_pText )
	{
		AddChild(m_pText);
		XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
	}
}

cRegContact::~cRegContact()
{
}

void cRegContact::Initialize()
{
    m_sDescription = wxT("Regular Contact");
    XS_SERIALIZE(m_sDescription, wxT("description"));
    SetConnectToVertex(false);

    AcceptConnection(wxT("All"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
	AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

	m_pText = new wxSFEditTextShape();
    if(m_pText)
    {
        m_pText->SetText(wxT("label"));

        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

		m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);

		SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}

bool cRegContact::OnKey(int key)
{
    switch(key)
    {
        case WXK_F2:
            if(m_pText->IsActive() && m_pText->IsVisible())
            {
                m_pText->EditLabel();
            }
            break;

        default:
            break;
    }

    return true;
}

#include "../gui/wx_pch.h"
#include "RegCoil.h"

XS_IMPLEMENT_CLONABLE_CLASS(cRegCoil, wxSFCircleShape);

cRegCoil::cRegCoil()
{
    Initialize();
}

cRegCoil::cRegCoil(const cRegCoil& obj) : wxSFCircleShape(obj)
{
    m_pText = (wxSFEditTextShape*)obj.m_pText->Clone();
	if( m_pText )
	{
		AddChild(m_pText);
		XS_SERIALIZE_DYNAMIC_OBJECT_NO_CREATE(m_pText, wxT("label"));
	}
}

cRegCoil::~cRegCoil()
{
}

void cRegCoil::Initialize()
{
    m_nRectSize = wxRealPoint(100, 100);
    RemoveStyle(sfsSIZE_CHANGE);

    m_sDescription = wxT("Regular Coil");
    XS_SERIALIZE(m_sDescription, wxT("description"));

    AcceptConnection(wxT("All"));
    AcceptSrcNeighbour(wxT("All"));
    AcceptTrgNeighbour(wxT("All"));

    AddConnectionPoint( wxSFConnectionPoint::cpCENTERLEFT );
	AddConnectionPoint( wxSFConnectionPoint::cpCENTERRIGHT );

	m_pText = new wxSFEditTextShape();
    if(m_pText)
    {
        m_pText->SetText(wxT("label"));

        m_pText->SetVAlign(wxSFShapeBase::valignBOTTOM);
        m_pText->SetVBorder(-35);
        m_pText->SetHAlign(wxSFShapeBase::halignCENTER);

        m_pText->Scale(1.5, 1.5, true);

		m_pText->SetStyle(sfsHOVERING | sfsPROCESS_DEL | sfsPROPAGATE_DRAGGING | sfsPROPAGATE_SELECTION);

		SF_ADD_COMPONENT( m_pText, wxT("label") );
    }
}

bool cRegCoil::OnKey(int key)
{
    switch(key)
    {
        case WXK_F2:
            if(m_pText->IsActive() && m_pText->IsVisible())
            {
                m_pText->EditLabel();
            }
            break;

        default:
            break;
    }

    return true;
}

#ifndef IDS_H_INCLUDED
#define IDS_H_INCLUDED

enum APPIDS
{
    wxIDT_GRID,
    wxIDT_SHADOW,
	wxIDT_GC,
    wxIDT_TOOL,

    wxIDT_DESIGN,
    wxIDT_LIMIT,
    wxIDT_REGCONTACT,
    wxIDT_NOTCONTACT,
    wxIDT_REGCOIL,
    wxIDT_LINE
};

#endif // IDS_H_INCLUDED

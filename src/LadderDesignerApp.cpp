#include "gui/wx_pch.h"

#include "LadderDesignerMain.h"
#include "LadderDesignerApp.h"

// wxwindows app
IMPLEMENT_APP( MainApp )

// class info
IMPLEMENT_DYNAMIC_CLASS( MainApp, wxApp )

bool MainApp::OnInit()
{
    // main frame
    GUI_MainFrm *frame = new GUI_MainFrm( NULL );
    SetTopWindow( frame );
    frame->Show( true );

    return true;
}

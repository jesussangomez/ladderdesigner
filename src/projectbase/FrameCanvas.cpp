#include "../gui/wx_pch.h"

#include "FrameCanvas.h"
#include "../LadderDesignerMain.h"
#include "../LadderDesignerApp.h"
#include "../shapes/RegContact.h"
#include "../shapes/NotContact.h"
#include "../shapes/RegCoil.h"
#include "../shapes/Limit.h"

#include <wx/textdlg.h>
#include <wx/filename.h>

FrameCanvas::FrameCanvas(wxSFDiagramManager* manager, wxWindow* parent, wxWindowID id)
: wxSFShapeCanvas(manager, parent, id, wxDefaultPosition, wxDefaultSize, wxHSCROLL | wxVSCROLL | wxSTATIC_BORDER)
{
    // get pointer to main application frame
	m_pParentFrame = (GUI_MainFrm*)wxGetApp().GetTopWindow();

	// initialize grid
	AddStyle(sfsGRID_USE);
	AddStyle(sfsGRID_SHOW);
	// distances between grid lines can be modified via following function:
	SetGridLineMult(5);
	// grid line style can be set as follows:
	SetGridStyle(wxSHORT_DASH);

	SetCanvasColour(wxColour(255, 255, 255));

	// process mouse wheel if needed
	AddStyle(sfsPROCESS_MOUSEWHEEL);
	// set scale boundaries aplied on mouse wheel scale change
	SetMinScale(0.5);
	SetMaxScale(2);
    SetScale(0.5);

	// set accepted shapes
	GetDiagramManager()->ClearAcceptedShapes();
	GetDiagramManager()->AcceptShape(wxT("All"));

	GetHistoryManager().SetMode(wxSFCanvasHistory::histUSE_SERIALIZATION);

	SaveCanvasState();
}

FrameCanvas::~FrameCanvas(void)
{
}

void FrameCanvas::OnLeftDown(wxMouseEvent& event)
{
    wxSFShapeBase *pShape = NULL;
//    ShapeList lstShapes;
//    wxSFShapeBase *pChild;

	switch(m_pParentFrame->GetToolMode())
	{
        case GUI_MainFrm::modeLIMIT:
        {
            // create new composite shape
            pShape = GetDiagramManager()->AddShape(new cLimit(), NULL, event.GetPosition(), sfINITIALIZE, sfDONT_SAVE_STATE);
            //pShape = GetDiagramManager()->AddShape(new cLimit(), NULL, event.GetPosition() + wxPoint(600,0), sfINITIALIZE, sfDONT_SAVE_STATE);

        }
        break;

        case GUI_MainFrm::modeREGCONTACT:
		{
			// create new composite shape
            pShape = GetDiagramManager()->AddShape(new cRegContact(), NULL, event.GetPosition(), sfINITIALIZE, sfDONT_SAVE_STATE);
            //pShape->GetChildShapes(sfANY, lstShapes, sfRECURSIVE);
            //ShapeList::compatibility_iterator node = lstShapes.GetFirst();
            //pChild = node->GetData();
            //GUI_MainFrm::Log( wxString::Format( wxT("%s "), pChild->GetProperty(wxT("text"))->AsInt() ) );
		}
		break;

        case GUI_MainFrm::modeNOTCONTACT:
		{
            pShape = GetDiagramManager()->AddShape(new cNotContact(), NULL, event.GetPosition(), sfINITIALIZE, sfDONT_SAVE_STATE);
        }
		break;

		case GUI_MainFrm::modeREGCOIL:
		{
            pShape = GetDiagramManager()->AddShape(new cRegCoil(), NULL, event.GetPosition(), sfINITIALIZE, sfDONT_SAVE_STATE);
		}
		break;

		case GUI_MainFrm::modeLINE:
        {
            if(GetMode() == modeREADY)
            {
                StartInteractiveConnection(CLASSINFO(wxSFOrthoLineShape), event.GetPosition());
            }
            else
                wxSFShapeCanvas::OnLeftDown(event);
        }
        break;

        default:
            // do default actions
            wxSFShapeCanvas::OnLeftDown(event);
	}

	if( pShape )
	{
	    ShowShadows(m_pParentFrame->m_fShowShadows, shadowALL);

	    SaveCanvasState();

        if(!event.ControlDown())
        {
            m_pParentFrame->SetToolMode(GUI_MainFrm::modeDESIGN);
        }

        pShape->Refresh();
	}
}

void FrameCanvas::OnRightDown(wxMouseEvent& event)
{
    // try to find shape under cursor
	wxSFShapeBase* pShape = GetShapeUnderCursor();
	// eventualy you can use:
    //wxSFShapeBase *pShape = GetShapeAtPosition(DP2LP(event.GetPosition()), 1, searchBOTH);

    // print out information about the shape (if found)
    if(pShape)
    {
        ShapeList lstShapes;
        wxString msg;
        wxSFShapeBase *pChild;
        int counter;

        // show basic info
        msg.Printf(wxT("Class name: %s, ID: %d\n"), pShape->GetClassInfo()->GetClassName(), pShape->GetId());

        // show info about shape's children
       counter = 1;
       pShape->GetChildShapes(sfANY, lstShapes, sfRECURSIVE);
       if( !lstShapes.IsEmpty() )
       {
           msg += wxT("\nChildren:\n");
           ShapeList::compatibility_iterator node = lstShapes.GetFirst();
           while(node)
           {
               pChild = node->GetData();

               msg += wxString::Format(wxT("%d. Class name: %s, ID: %d, Label: %s\n"), counter, pChild->GetClassInfo()->GetClassName(), pChild->GetId(), pChild->GetProperty(wxT("text"))->AsInt());
               counter++;

               node = node->GetNext();
           }
       }

        // show info about shape's neighbours
        counter = 1;
        lstShapes.Clear();
        pShape->GetNeighbours(lstShapes, CLASSINFO(wxSFLineShape), wxSFShapeBase::lineBOTH, sfINDIRECT);
        if( !lstShapes.IsEmpty() )
        {
            msg += wxT("\nNeighbours:\n");

            ShapeList::compatibility_iterator node = lstShapes.GetFirst();
            while(node)
            {
                pChild = node->GetData();

                msg += wxString::Format(wxT("%d. Class name: %s, ID: %d\n"), counter, pChild->GetClassInfo()->GetClassName(), pChild->GetId());
                counter++;

                node = node->GetNext();
            }
        }

        // show message
        wxMessageBox(msg, wxT("wxShapeFramework"), wxOK | wxICON_INFORMATION);
    }
    else
        wxMessageBox(wxT("No shape found on this position."), wxT("wxShapeFramework"), wxOK | wxICON_INFORMATION);

    // call default handler
    wxSFShapeCanvas::OnRightDown(event);
}

void FrameCanvas::OnKeyDown(wxKeyEvent& event)
{
	switch(event.GetKeyCode())
	{
	case WXK_ESCAPE:
		m_pParentFrame->SetToolMode(GUI_MainFrm::modeDESIGN);
		break;
	}
	// do default actions
	wxSFShapeCanvas::OnKeyDown(event);
}

void FrameCanvas::OnConnectionFinished(wxSFLineShape* connection)
{
    if(connection)
    {
		// the line's ending style can be set like this:
        //connection->SetTrgArrow(CLASSINFO(wxSFSolidArrow));
        connection->SetTrgArrow(CLASSINFO(wxSFSolidArrow));
        // also wxSFOpenArrow, wxSFDiamondArrow and wxSFCircleArrow styles are available.
		connection->SetSrcArrow(CLASSINFO(wxSFCircleArrow));

        connection->AcceptChild(wxT("wxSFTextShape"));
        connection->AcceptChild(wxT("wxSFEditTextShape"));

		connection->AcceptConnection(wxT("All"));
        connection->AcceptSrcNeighbour(wxT("All"));
        connection->AcceptTrgNeighbour(wxT("All"));

		connection->SetDockPoint(sfdvLINESHAPE_DOCKPOINT_CENTER);

		m_pParentFrame->SetToolMode(GUI_MainFrm::modeDESIGN);
    }
}

void FrameCanvas::OnMouseWheel(wxMouseEvent& event)
{
	wxSFShapeCanvas::OnMouseWheel(event);
}


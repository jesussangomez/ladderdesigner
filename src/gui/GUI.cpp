///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "wx_pch.h"

#include "GUI.h"

#include "../res/Compile.xpm"
#include "../res/GC.xpm"
#include "../res/Gen.xpm"
#include "../res/Grid.xpm"
#include "../res/Limit.xpm"
#include "../res/NotContact.xpm"
#include "../res/OrthoLine.xpm"
#include "../res/Program.xpm"
#include "../res/RegCoil.xpm"
#include "../res/RegContact.xpm"
#include "../res/Shadow.xpm"
#include "../res/Tool.xpm"

///////////////////////////////////////////////////////////////////////////

BEGIN_EVENT_TABLE( _MainFrm, wxFrame )
	EVT_MENU( wxID_NEW, _MainFrm::_wxFB_OnNew )
	EVT_MENU( wxID_OPEN, _MainFrm::_wxFB_OnOpen )
	EVT_MENU( wxID_SAVE, _MainFrm::_wxFB_OnSave )
	EVT_MENU( wxID_SAVEAS, _MainFrm::_wxFB_OnSaveAs )
	EVT_MENU( wxID_EXPORT, _MainFrm::_wxFB_OnExportFile )
	EVT_MENU( wxID_PRINT, _MainFrm::_wxFB_OnPrint )
	EVT_MENU( wxID_PREVIEW, _MainFrm::_wxFB_OnPrintPreview )
	EVT_MENU( wxID_SETUP, _MainFrm::_wxFB_OnPageSetup )
	EVT_MENU( wxID_QUIT, _MainFrm::_wxFB_OnQuit )
	EVT_MENU( wxID_UNDO, _MainFrm::_wxFB_OnUndo )
	EVT_UPDATE_UI( wxID_UNDO, _MainFrm::_wxFB_OnUpdateUndo )
	EVT_MENU( wxID_REDO, _MainFrm::_wxFB_OnRedo )
	EVT_UPDATE_UI( wxID_REDO, _MainFrm::_wxFB_OnUpdateRedo )
	EVT_MENU( wxID_SELECT, _MainFrm::_wxFB_OnSelectAll )
	EVT_MENU( wxID_COPY, _MainFrm::_wxFB_OnCopy )
	EVT_UPDATE_UI( wxID_COPY, _MainFrm::_wxFB_OnUpdateCopy )
	EVT_MENU( wxID_CUT, _MainFrm::_wxFB_OnCut )
	EVT_UPDATE_UI( wxID_CUT, _MainFrm::_wxFB_OnUpdateCut )
	EVT_MENU( wxID_PASTE, _MainFrm::_wxFB_OnPaste )
	EVT_UPDATE_UI( wxID_PASTE, _MainFrm::_wxFB_OnUpdatePaste )
	EVT_MENU( wxID_ABOUT, _MainFrm::_wxFB_OnAbout )
	EVT_TOOL( wxID_NEW, _MainFrm::_wxFB_OnNew )
	EVT_TOOL( wxID_OPEN, _MainFrm::_wxFB_OnOpen )
	EVT_TOOL( wxID_SAVE, _MainFrm::_wxFB_OnSave )
	EVT_TOOL( wxID_COPY, _MainFrm::_wxFB_OnCopy )
	EVT_UPDATE_UI( wxID_COPY, _MainFrm::_wxFB_OnUpdateCopy )
	EVT_TOOL( wxID_CUT, _MainFrm::_wxFB_OnCut )
	EVT_UPDATE_UI( wxID_CUT, _MainFrm::_wxFB_OnUpdateCut )
	EVT_TOOL( wxID_PASTE, _MainFrm::_wxFB_OnPaste )
	EVT_UPDATE_UI( wxID_PASTE, _MainFrm::_wxFB_OnUpdatePaste )
	EVT_TOOL( wxID_UNDO, _MainFrm::_wxFB_OnUndo )
	EVT_UPDATE_UI( wxID_UNDO, _MainFrm::_wxFB_OnUpdateUndo )
	EVT_TOOL( wxID_REDO, _MainFrm::_wxFB_OnRedo )
	EVT_UPDATE_UI( wxID_REDO, _MainFrm::_wxFB_OnUpdateRedo )
	EVT_TOOL( wxIDT_GRID, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_GRID, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_SHADOW, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_SHADOW, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_GC, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_GC, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_DESIGN, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_DESIGN, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_LINE, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_LINE, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_LIMIT, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_LIMIT, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_REGCONTACT, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_REGCONTACT, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_NOTCONTACT, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_NOTCONTACT, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_REGCOIL, _MainFrm::_wxFB_OnTool )
	EVT_UPDATE_UI( wxIDT_REGCOIL, _MainFrm::_wxFB_OnUpdateTool )
	EVT_TOOL( wxIDT_GEN, _MainFrm::_wxFB_GenTool )
	EVT_TOOL( wxIDT_COMPILE, _MainFrm::_wxFB_CompileTool )
	EVT_TOOL( wxIDT_PROGRAM, _MainFrm::_wxFB_ProgramTool )
END_EVENT_TABLE()

_MainFrm::_MainFrm( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 640,480 ), wxDefaultSize );
	
	m_MenuBar = new wxMenuBar( 0 );
	m_FileMenu = new wxMenu();
	wxMenuItem* m_NewFile;
	m_NewFile = new wxMenuItem( m_FileMenu, wxID_NEW, wxString( wxT("&New") ) + wxT('\t') + wxT("Ctrl+N"), wxT("Create an empty project"), wxITEM_NORMAL );
	m_FileMenu->Append( m_NewFile );
	
	wxMenuItem* m_OpenFile;
	m_OpenFile = new wxMenuItem( m_FileMenu, wxID_OPEN, wxString( wxT("&Open") ) + wxT('\t') + wxT("Ctrl+O"), wxT("Load a chart from XML file"), wxITEM_NORMAL );
	m_FileMenu->Append( m_OpenFile );
	
	wxMenuItem* m_SaveFile;
	m_SaveFile = new wxMenuItem( m_FileMenu, wxID_SAVE, wxString( wxT("&Save") ) + wxT('\t') + wxT("Ctrl+S"), wxT("Save the chart to XML file"), wxITEM_NORMAL );
	m_FileMenu->Append( m_SaveFile );
	
	wxMenuItem* m_SaveAsFile;
	m_SaveAsFile = new wxMenuItem( m_FileMenu, wxID_SAVEAS, wxString( wxT("Save as...") ) + wxT('\t') + wxT("Ctrl+Shift+S"), wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_SaveAsFile );
	
	m_FileMenu->AppendSeparator();
	
	wxMenuItem* m_ExportFile;
	m_ExportFile = new wxMenuItem( m_FileMenu, wxID_EXPORT, wxString( wxT("Export to image...") ) , wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_ExportFile );
	
	m_FileMenu->AppendSeparator();
	
	wxMenuItem* m_PrintFile;
	m_PrintFile = new wxMenuItem( m_FileMenu, wxID_PRINT, wxString( wxT("Print...") ) + wxT('\t') + wxT("Ctrl+P"), wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_PrintFile );
	
	wxMenuItem* m_PreviewFile;
	m_PreviewFile = new wxMenuItem( m_FileMenu, wxID_PREVIEW, wxString( wxT("Print preview...") ) + wxT('\t') + wxT("Alt+P"), wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_PreviewFile );
	
	wxMenuItem* m_SetupFile;
	m_SetupFile = new wxMenuItem( m_FileMenu, wxID_SETUP, wxString( wxT("Page setup...") ) , wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_SetupFile );
	
	m_FileMenu->AppendSeparator();
	
	wxMenuItem* m_QuitMenu;
	m_QuitMenu = new wxMenuItem( m_FileMenu, wxID_QUIT, wxString( wxT("&Quit") ) + wxT('\t') + wxT("Ctrl+Q"), wxEmptyString, wxITEM_NORMAL );
	m_FileMenu->Append( m_QuitMenu );
	
	m_MenuBar->Append( m_FileMenu, wxT("&File") ); 
	
	m_EditMenu = new wxMenu();
	wxMenuItem* m_UndoEdit;
	m_UndoEdit = new wxMenuItem( m_EditMenu, wxID_UNDO, wxString( wxT("&Undo") ) + wxT('\t') + wxT("Ctrl+Z"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_UndoEdit );
	
	wxMenuItem* m_RedoEdit;
	m_RedoEdit = new wxMenuItem( m_EditMenu, wxID_REDO, wxString( wxT("&Redo") ) + wxT('\t') + wxT("Ctrl+Y"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_RedoEdit );
	
	m_EditMenu->AppendSeparator();
	
	wxMenuItem* m_SelectAllEdit;
	m_SelectAllEdit = new wxMenuItem( m_EditMenu, wxID_SELECT, wxString( wxT("Select all") ) + wxT('\t') + wxT("Ctrl+A"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_SelectAllEdit );
	
	m_EditMenu->AppendSeparator();
	
	wxMenuItem* m_CopyEdit;
	m_CopyEdit = new wxMenuItem( m_EditMenu, wxID_COPY, wxString( wxT("&Copy") ) + wxT('\t') + wxT("Ctrl+C"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_CopyEdit );
	
	wxMenuItem* m_CutEdit;
	m_CutEdit = new wxMenuItem( m_EditMenu, wxID_CUT, wxString( wxT("&Cut") ) + wxT('\t') + wxT("Ctrl+X"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_CutEdit );
	
	wxMenuItem* m_PasteEdit;
	m_PasteEdit = new wxMenuItem( m_EditMenu, wxID_PASTE, wxString( wxT("&Paste") ) + wxT('\t') + wxT("Ctrl+V"), wxEmptyString, wxITEM_NORMAL );
	m_EditMenu->Append( m_PasteEdit );
	
	m_MenuBar->Append( m_EditMenu, wxT("&Edit") ); 
	
	m_HelpMenu = new wxMenu();
	wxMenuItem* m_AboutHelp;
	m_AboutHelp = new wxMenuItem( m_HelpMenu, wxID_ABOUT, wxString( wxT("About...") ) + wxT('\t') + wxT("F1"), wxEmptyString, wxITEM_NORMAL );
	m_HelpMenu->Append( m_AboutHelp );
	
	m_MenuBar->Append( m_HelpMenu, wxT("&Help") ); 
	
	this->SetMenuBar( m_MenuBar );
	
	m_ToolBar = this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY ); 
	m_ToolBar->AddTool( wxID_NEW, wxT("New"), wxArtProvider::GetBitmap( wxART_NEW, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxT("Create an empty project"), NULL ); 
	
	m_ToolBar->AddTool( wxID_OPEN, wxT("Open"), wxArtProvider::GetBitmap( wxART_FILE_OPEN, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxT("Load a chart from XML file"), NULL ); 
	
	m_ToolBar->AddTool( wxID_SAVE, wxT("Save"), wxArtProvider::GetBitmap( wxART_FILE_SAVE, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxT("Save file..."), wxEmptyString, NULL ); 
	
	m_ToolBar->AddSeparator(); 
	
	m_ToolBar->AddTool( wxID_COPY, wxT("Copy"), wxArtProvider::GetBitmap( wxART_COPY, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddTool( wxID_CUT, wxT("Cut"), wxArtProvider::GetBitmap( wxART_CUT, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddTool( wxID_PASTE, wxT("Paste"), wxArtProvider::GetBitmap( wxART_PASTE, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddSeparator(); 
	
	m_ToolBar->AddTool( wxID_UNDO, wxT("Undo"), wxArtProvider::GetBitmap( wxART_UNDO, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddTool( wxID_REDO, wxT("Redo"), wxArtProvider::GetBitmap( wxART_REDO, wxART_TOOLBAR ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddSeparator(); 
	
	m_ToolBar->AddTool( wxIDT_GRID, wxT("Grid"), wxBitmap( Grid_xpm ), wxNullBitmap, wxITEM_CHECK, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddTool( wxIDT_SHADOW, wxT("Shadow"), wxBitmap( Shadow_xpm ), wxNullBitmap, wxITEM_CHECK, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->AddTool( wxIDT_GC, wxT("GC"), wxBitmap( GC_xpm ), wxNullBitmap, wxITEM_CHECK, wxEmptyString, wxEmptyString, NULL ); 
	
	m_ToolBar->Realize(); 
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );
	
	m_LadderTools = new wxToolBar( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL );
	m_LadderTools->SetToolPacking( 0 );
	m_LadderTools->AddTool( wxIDT_DESIGN, wxT("Design"), wxBitmap( Tool_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_LINE, wxT("Line"), wxBitmap( OrthoLine_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_LIMIT, wxT("Limit"), wxBitmap( Limit_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_REGCONTACT, wxT("Regular Contact"), wxBitmap( RegContact_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_NOTCONTACT, wxT("Not Contact"), wxBitmap( NotContact_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_REGCOIL, wxT("Regular Coil"), wxBitmap( RegCoil_xpm ), wxNullBitmap, wxITEM_RADIO, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddSeparator(); 
	
	m_LadderTools->AddTool( wxIDT_GEN, wxT("tool"), wxBitmap( Gen_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_COMPILE, wxT("tool"), wxBitmap( Compile_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->AddTool( wxIDT_PROGRAM, wxT("tool"), wxBitmap( Program_xpm ), wxNullBitmap, wxITEM_NORMAL, wxEmptyString, wxEmptyString, NULL ); 
	
	m_LadderTools->Realize(); 
	
	bSizer2->Add( m_LadderTools, 0, wxALL|wxEXPAND, 0 );
	
	wxFlexGridSizer* MainSizer;
	MainSizer = new wxFlexGridSizer( 2, 2, 0, 0 );
	MainSizer->AddGrowableCol( 0 );
	MainSizer->AddGrowableRow( 0 );
	MainSizer->SetFlexibleDirection( wxHORIZONTAL );
	MainSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_CanvasPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_CanvasPanel->SetExtraStyle( wxWS_EX_BLOCK_EVENTS );
	
	m_CanvasSizer = new wxBoxSizer( wxVERTICAL );
	
	
	m_CanvasPanel->SetSizer( m_CanvasSizer );
	m_CanvasPanel->Layout();
	m_CanvasSizer->Fit( m_CanvasPanel );
	MainSizer->Add( m_CanvasPanel, 1, wxALL|wxEXPAND, 1 );
	
	m_TextEditor = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 500,-1 ), wxTE_MULTILINE );
	MainSizer->Add( m_TextEditor, 0, wxALL|wxEXPAND, 0 );
	
	
	bSizer2->Add( MainSizer, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer2 );
	this->Layout();
	m_StatusBar = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
}

_MainFrm::~_MainFrm()
{
}

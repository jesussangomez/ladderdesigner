///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct  8 2012)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __GUI_H__
#define __GUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/toolbar.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class _MainFrm
///////////////////////////////////////////////////////////////////////////////
class _MainFrm : public wxFrame 
{
	DECLARE_EVENT_TABLE()
	private:
		
		// Private event handlers
		void _wxFB_OnNew( wxCommandEvent& event ){ OnNew( event ); }
		void _wxFB_OnOpen( wxCommandEvent& event ){ OnOpen( event ); }
		void _wxFB_OnSave( wxCommandEvent& event ){ OnSave( event ); }
		void _wxFB_OnSaveAs( wxCommandEvent& event ){ OnSaveAs( event ); }
		void _wxFB_OnExportFile( wxCommandEvent& event ){ OnExportFile( event ); }
		void _wxFB_OnPrint( wxCommandEvent& event ){ OnPrint( event ); }
		void _wxFB_OnPrintPreview( wxCommandEvent& event ){ OnPrintPreview( event ); }
		void _wxFB_OnPageSetup( wxCommandEvent& event ){ OnPageSetup( event ); }
		void _wxFB_OnQuit( wxCommandEvent& event ){ OnQuit( event ); }
		void _wxFB_OnUndo( wxCommandEvent& event ){ OnUndo( event ); }
		void _wxFB_OnUpdateUndo( wxUpdateUIEvent& event ){ OnUpdateUndo( event ); }
		void _wxFB_OnRedo( wxCommandEvent& event ){ OnRedo( event ); }
		void _wxFB_OnUpdateRedo( wxUpdateUIEvent& event ){ OnUpdateRedo( event ); }
		void _wxFB_OnSelectAll( wxCommandEvent& event ){ OnSelectAll( event ); }
		void _wxFB_OnCopy( wxCommandEvent& event ){ OnCopy( event ); }
		void _wxFB_OnUpdateCopy( wxUpdateUIEvent& event ){ OnUpdateCopy( event ); }
		void _wxFB_OnCut( wxCommandEvent& event ){ OnCut( event ); }
		void _wxFB_OnUpdateCut( wxUpdateUIEvent& event ){ OnUpdateCut( event ); }
		void _wxFB_OnPaste( wxCommandEvent& event ){ OnPaste( event ); }
		void _wxFB_OnUpdatePaste( wxUpdateUIEvent& event ){ OnUpdatePaste( event ); }
		void _wxFB_OnAbout( wxCommandEvent& event ){ OnAbout( event ); }
		void _wxFB_OnTool( wxCommandEvent& event ){ OnTool( event ); }
		void _wxFB_OnUpdateTool( wxUpdateUIEvent& event ){ OnUpdateTool( event ); }
		void _wxFB_GenTool( wxCommandEvent& event ){ GenTool( event ); }
		void _wxFB_CompileTool( wxCommandEvent& event ){ CompileTool( event ); }
		void _wxFB_ProgramTool( wxCommandEvent& event ){ ProgramTool( event ); }
		
	
	protected:
		enum
		{
			wxID_EXPORT = 1000,
			wxID_QUIT,
			wxID_SELECT,
			wxIDT_GRID,
			wxIDT_SHADOW,
			wxIDT_GC,
			wxIDT_DESIGN,
			wxIDT_LINE,
			wxIDT_LIMIT,
			wxIDT_REGCONTACT,
			wxIDT_NOTCONTACT,
			wxIDT_REGCOIL,
			wxIDT_GEN,
			wxIDT_COMPILE,
			wxIDT_PROGRAM
		};
		
		wxMenuBar* m_MenuBar;
		wxMenu* m_FileMenu;
		wxMenu* m_EditMenu;
		wxMenu* m_HelpMenu;
		wxToolBar* m_ToolBar;
		wxToolBar* m_LadderTools;
		wxPanel* m_CanvasPanel;
		wxTextCtrl* m_TextEditor;
		wxStatusBar* m_StatusBar;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnNew( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnOpen( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSave( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveAs( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnExportFile( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPrint( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPrintPreview( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPageSetup( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnQuit( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUndo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateUndo( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnRedo( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateRedo( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnSelectAll( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCopy( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateCopy( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnCut( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateCut( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnPaste( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdatePaste( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void OnAbout( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTool( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateTool( wxUpdateUIEvent& event ) { event.Skip(); }
		virtual void GenTool( wxCommandEvent& event ) { event.Skip(); }
		virtual void CompileTool( wxCommandEvent& event ) { event.Skip(); }
		virtual void ProgramTool( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxBoxSizer* m_CanvasSizer;
		
		_MainFrm( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("LadderDesigner"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 705,539 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~_MainFrm();
	
};

#endif //__GUI_H__

#include "LadderDesignerMain.h"
#include "Ids.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>

using namespace std;

GUI_MainFrm::GUI_MainFrm( wxWindow* parent )
:
_MainFrm( parent )
{
    wxInitAllImageHandlers();

    m_ShapeCanvas = new FrameCanvas(&m_DiagramManager, m_CanvasPanel, wxID_ANY);
	m_CanvasSizer->Add(m_ShapeCanvas, 1, wxEXPAND, 0);
	m_CanvasPanel->Layout();

	m_StatusBar->SetStatusText(wxT("Ready"));

	m_nToolMode = modeDESIGN;
	m_fShowShadows = false;

	#ifdef __linux__
        wxSFShapeCanvas::EnableGC( true );
        m_fShowGrid = true;
    #elif _WIN32 || defined _WIN64
        wxSFShapeCanvas::EnableGC( false );
        m_fShowGrid = false;
        m_ShapeCanvas->RemoveStyle(wxSFShapeCanvas::sfsGRID_SHOW);
        m_ShapeCanvas->RemoveStyle(wxSFShapeCanvas::sfsGRID_USE);
    #endif
}

void GUI_MainFrm::OnNew( wxCommandEvent& event )
{
    if(wxMessageBox(wxT("Current chart will be lost. Do you want to proceed?"), wxT("ShapeFramework"), wxYES_NO | wxICON_QUESTION) == wxYES)
	{
		m_DiagramManager.Clear();
		m_ShapeCanvas->ClearCanvasHistory();

        m_DiagramManager.ClearAcceptedShapes();
        m_DiagramManager.AcceptShape(wxT("All"));

        m_ShapeCanvas->SaveCanvasState();
		m_ShapeCanvas->Refresh();

		CleanLog();
	}
}

void GUI_MainFrm::OnOpen( wxCommandEvent& event )
{
    wxFileDialog dlg(this, wxT("Open project..."), wxGetCwd(), wxT(""), wxT("LadderDesigner (*.ldp)|*.ldp"), wxOPEN | wxFD_FILE_MUST_EXIST);

	if(dlg.ShowModal() == wxID_OK)
	{
		m_ShapeCanvas->LoadCanvas(dlg.GetPath());
	}
}

void GUI_MainFrm::OnSave( wxCommandEvent& event )
{
    wxFileDialog dlg(this, wxT("Save project..."), wxGetCwd(), wxT(""), wxT("LadderDesigner project (*.ldp)|*.ldp"), wxSAVE | wxFD_OVERWRITE_PROMPT);

	if(dlg.ShowModal() == wxID_OK)
	{
		m_ShapeCanvas->SaveCanvas(dlg.GetPath());

		//wxMessageBox(wxString::Format(wxT("The chart has been saved to '%s'."), dlg.GetPath().GetData()), wxT("ShapeFramework"));
	}
}

void GUI_MainFrm::OnSaveAs( wxCommandEvent& event )
{
    wxFileDialog dlg(this, wxT("Save project..."), wxGetCwd(), wxT(""), wxT("LadderDesigner project (*.ldp)|*.ldp"), wxSAVE | wxFD_OVERWRITE_PROMPT);

	if(dlg.ShowModal() == wxID_OK)
	{
		m_ShapeCanvas->SaveCanvas(dlg.GetPath());

		//wxMessageBox(wxString::Format(wxT("The chart has been saved to '%s'."), dlg.GetPath().GetData()), wxT("ShapeFramework"));
	}
}

void GUI_MainFrm::OnExportFile( wxCommandEvent& event )
{
    wxFileDialog dlg(this, wxT("Export canvas to image..."), wxGetCwd(), wxT(""),
		wxT("BMP Files (*.bmp)|*.bmp|GIF Files (*.gif)|(*.gif)|XPM Files (*.xpm)|*.xpm|PNG Files (*.png)|*.png|JPEG Files (*.jpg)|*.jpg"), wxSAVE);

	if(dlg.ShowModal() == wxID_OK)
	{
		wxBitmapType type = wxBITMAP_TYPE_ANY;

		switch( dlg.GetFilterIndex() )
		{
			case 0:
				type = wxBITMAP_TYPE_BMP;
				break;
			case 1:
				type = wxBITMAP_TYPE_GIF;
				break;
			case 2:
				type = wxBITMAP_TYPE_XPM;
				break;
			case 3:
				type = wxBITMAP_TYPE_PNG;
				break;
			case 4:
				type = wxBITMAP_TYPE_JPEG;
				break;
		}

        m_ShapeCanvas->SaveCanvasToImage( dlg.GetPath(), type, sfWITH_BACKGROUND );
	}
}

void GUI_MainFrm::OnPrint( wxCommandEvent& event )
{
    m_ShapeCanvas->Print();
}

void GUI_MainFrm::OnPrintPreview( wxCommandEvent& event )
{
    m_ShapeCanvas->PrintPreview();
}

void GUI_MainFrm::OnPageSetup( wxCommandEvent& event )
{
    m_ShapeCanvas->PageSetup();
}

void GUI_MainFrm::OnQuit( wxCommandEvent& event )
{
    CleanUp();
}

void GUI_MainFrm::OnUndo( wxCommandEvent& event )
{
    m_ShapeCanvas->Undo();
}

void GUI_MainFrm::OnUpdateUndo( wxUpdateUIEvent& event )
{
    if( m_ShapeCanvas ) event.Enable(m_ShapeCanvas->CanUndo());
}

void GUI_MainFrm::OnRedo( wxCommandEvent& event )
{
    m_ShapeCanvas->Redo();
}

void GUI_MainFrm::OnUpdateRedo( wxUpdateUIEvent& event )
{
    if( m_ShapeCanvas ) event.Enable(m_ShapeCanvas->CanRedo());
}

void GUI_MainFrm::OnSelectAll( wxCommandEvent& event )
{
    m_ShapeCanvas->SelectAll();
}

void GUI_MainFrm::OnCopy( wxCommandEvent& event )
{
    m_ShapeCanvas->Copy();
}

void GUI_MainFrm::OnUpdateCopy( wxUpdateUIEvent& event )
{
    if( m_ShapeCanvas ) event.Enable(m_ShapeCanvas->CanCopy());
}

void GUI_MainFrm::OnCut( wxCommandEvent& event )
{
    m_ShapeCanvas->Cut();
}

void GUI_MainFrm::OnUpdateCut( wxUpdateUIEvent& event )
{
    if( m_ShapeCanvas ) event.Enable(m_ShapeCanvas->CanCut());
}

void GUI_MainFrm::OnPaste( wxCommandEvent& event )
{
    m_ShapeCanvas->Paste();
}

void GUI_MainFrm::OnUpdatePaste( wxUpdateUIEvent& event )
{
    if( m_ShapeCanvas ) event.Enable(m_ShapeCanvas->CanPaste());
}

void GUI_MainFrm::OnAbout( wxCommandEvent& event )
{
    wxMessageBox( wxT("Ladder Diagram Programming 13.04\n\nAuthor: Jesús Iván Sánchez Gómez\n\nEmail: jesus.sangomez@gmail.com") );
}

void GUI_MainFrm::OnTool( wxCommandEvent& event )
{
// TODO: Implement OnTool
    if(m_ShapeCanvas->GetMode() == FrameCanvas::modeCREATECONNECTION)m_ShapeCanvas->AbortInteractiveConnection();

    switch(event.GetId())
    {
        case wxIDT_GRID:
        	m_fShowGrid = !m_fShowGrid;
			if( m_fShowGrid )
			{
				m_ShapeCanvas->AddStyle(wxSFShapeCanvas::sfsGRID_SHOW);
				m_ShapeCanvas->AddStyle(wxSFShapeCanvas::sfsGRID_USE);
			}
			else
			{
				m_ShapeCanvas->RemoveStyle(wxSFShapeCanvas::sfsGRID_SHOW);
				m_ShapeCanvas->RemoveStyle(wxSFShapeCanvas::sfsGRID_USE);
			}
            m_ShapeCanvas->Refresh(false);
            break;

        case wxIDT_SHADOW:
        	m_fShowShadows = !m_fShowShadows;
        	m_ShapeCanvas->ShowShadows(m_fShowShadows, wxSFShapeCanvas::shadowALL);
            m_ShapeCanvas->Refresh(false);
            break;

        case wxIDT_GC:
			#if wxUSE_GRAPHICS_CONTEXT
                wxSFShapeCanvas::EnableGC( !wxSFShapeCanvas::IsGCEnabled() );
                m_DiagramManager.UpdateAll();
                m_ShapeCanvas->Refresh(false);
			#else
                wxMessageBox( wxT("Could not enable enhanced graphics context due to wxUSE_GRAPHICS_CONTEXT=0"), wxT("ShapeFramework"), wxOK | wxICON_WARNING );
			#endif
            break;

        case wxIDT_DESIGN:
            m_nToolMode = modeDESIGN;
            break;

        case wxIDT_LIMIT:
            m_nToolMode = modeLIMIT;
            break;

        case wxIDT_REGCONTACT:
            m_nToolMode = modeREGCONTACT;
            break;

        case wxIDT_NOTCONTACT:
            m_nToolMode = modeNOTCONTACT;
            break;

        case wxIDT_REGCOIL:
            m_nToolMode = modeREGCOIL;
            break;

        case wxIDT_LINE:
            m_nToolMode = modeLINE;
            break;

        default:
            event.Skip();
            break;
    }
}

void GUI_MainFrm::OnUpdateTool( wxUpdateUIEvent& event )
{
    switch(event.GetId())
    {
        case wxIDT_GRID:
        	event.Check(m_fShowGrid);
            break;

        case wxIDT_GC:
        	event.Check( wxSFShapeCanvas::IsGCEnabled() );
            break;

        case wxIDT_DESIGN:
            event.Check(m_nToolMode == modeDESIGN);
            break;

        case wxIDT_LIMIT:
            event.Check(m_nToolMode == modeLIMIT);
            break;

        case wxIDT_REGCONTACT:
            event.Check(m_nToolMode == modeREGCONTACT);
            break;

        case wxIDT_NOTCONTACT:
            event.Check(m_nToolMode == modeNOTCONTACT);
            break;

        case wxIDT_REGCOIL:
            event.Check(m_nToolMode == modeREGCOIL);
            break;

        case wxIDT_LINE:
            event.Check(m_nToolMode == modeLINE);
            break;

        default:
            event.Skip();
            break;
    }
}

void GUI_MainFrm::GenTool( wxCommandEvent& event )
{
    ShapeList lstShapes;
    wxSFShapeBase *pShape;
    char shapeLimit[] = "cLimit";
    char shapeCoil[] = "cRegCoil";
    char shapeNot[] = "cNotContact";
    char buffer[15];

    int counter = 0;
    vector< int > nShapes;
    vector< vector<int> > adjMatrix; // Array 2D Matriz de Adyacencia
    vector< string > codeBuffer;
    ostringstream auxBuf;

    CleanLog();

    m_DiagramManager.GetShapes(sfANY, lstShapes);

    if( !lstShapes.IsEmpty() )
    {
        ShapeList::compatibility_iterator node = lstShapes.GetFirst();
        while(node)
        {
            pShape = node->GetData();

            if(pShape->IsKindOf(CLASSINFO(wxSFPolygonShape)) || pShape->IsKindOf(CLASSINFO(wxSFCircleShape)))
            {
//                Log( wxString::Format( wxT("%d: %s\n"), counter, pShape->GetClassInfo()->GetClassName() ) );
                nShapes.push_back(pShape->GetId());
                counter++;
                wcstombs ( buffer, pShape->GetClassInfo()->GetClassName(), sizeof(buffer) );
                if(!strcmp(shapeCoil,buffer))
                {
                    auxBuf<<"ID_"<<pShape->GetId()<<" = ";
                    codeBuffer.push_back(auxBuf.str());
                    auxBuf.str(string());
                }
            }

            node = node->GetNext();
        }
    }

//    for(size_t i=0;i<nShapes.size();++i)
//        cout << i << ": " << nShapes[i] << endl;

//    for(size_t i=0;i<codeBuffer.size();++i)
//        cout << i << ": " << codeBuffer[i] << endl;

    // Array 2D
    adjMatrix.resize(nShapes.size());
    for (size_t i = 0; i < nShapes.size(); ++i)
        adjMatrix[i].resize(nShapes.size());

    for (size_t i = 0; i < nShapes.size(); ++i)
        for (size_t j = 0; j < nShapes.size(); ++j)
            adjMatrix[i][j] = 0;

    counter = 0;
    ShapeList lstNeighbours;
    wxSFShapeBase *pNeighbours;
    wxSFShapeBase *pAux = NULL;
    int countNB = 0;
    int eqCount = 0;
    int presentCount = -1;
    bool eqFlag = false;

    if( !lstShapes.IsEmpty() )
    {
        ShapeList::compatibility_iterator node = lstShapes.GetFirst();
        while(node)
        {
            pShape = node->GetData();

            if(pShape->IsKindOf(CLASSINFO(wxSFPolygonShape)) || pShape->IsKindOf(CLASSINFO(wxSFCircleShape)))
            {
                lstNeighbours.Clear();
                pShape->GetNeighbours(lstNeighbours, CLASSINFO(wxSFLineShape), wxSFShapeBase::lineSTARTING, sfINDIRECT);
                if( !lstNeighbours.IsEmpty() )
                {
                    ShapeList::compatibility_iterator node_neighbour = lstNeighbours.GetFirst();
                    while(node_neighbour)
                    {
                        pNeighbours = node_neighbour->GetData();

                        for (size_t j = 0; j < nShapes.size(); ++j)
                        {
                            if(pNeighbours->GetId() == nShapes[j])
                                adjMatrix[counter][j] = 1;
                        }
                        wcstombs ( buffer, pShape->GetClassInfo()->GetClassName(), sizeof(buffer) );
                        if(!strcmp(shapeLimit,buffer))
                        {
                            auxBuf<<"ID_"<<pNeighbours->GetId()<<" ";
                            codeBuffer[eqCount] += auxBuf.str();
//                            cout<<eqCount<<" "<<auxBuf.str()<<endl;
                            auxBuf.str(string());
                            eqCount++;
                        }
                        else
                        {
                            wcstombs ( buffer, pNeighbours->GetClassInfo()->GetClassName(), sizeof(buffer) );
                            if(strcmp(shapeLimit,buffer) && strcmp(shapeCoil,buffer)){
                                if(counter == presentCount){
                                    pAux = node_neighbour->GetData();
                                    eqFlag = true;
                                }
                                else{
                                    auxBuf<<" * ";
                                    presentCount = counter;
                                    wcstombs ( buffer, pNeighbours->GetClassInfo()->GetClassName(), sizeof(buffer) );
                                    if(strcmp(shapeNot,buffer))
                                        auxBuf<<"ID_"<<pNeighbours->GetId()<<" ";
                                    else
                                        auxBuf<<"/ID_"<<pNeighbours->GetId()<<" ";
                                    if(eqFlag){
                                        auxBuf<<" + ";
                                        wcstombs ( buffer, pAux->GetClassInfo()->GetClassName(), sizeof(buffer) );
                                        if(strcmp(shapeNot,buffer))
                                            auxBuf<<"ID_"<<pAux->GetId()<<" ";
                                        else
                                            auxBuf<<"/ID_"<<pAux->GetId()<<" ";
                                        eqFlag = false;
                                    }
                                }

                                codeBuffer[eqCount-1] += auxBuf.str();
//                                cout<<counter<<": "<<auxBuf.str()<<endl;
                                auxBuf.str(string());
                            }
                        }
                        //Log( wxString::Format( wxT("%d: %d %d\n"), countNB, pShape->GetId(), pNeighbours->GetId() ) );
                        countNB++;

                        node_neighbour = node_neighbour->GetNext();
                    }
                }

                counter++;
            }

            node = node->GetNext();
        }
    }

    Log( wxString::Format( wxT("init\n") ) );
    for(size_t i=0;i<codeBuffer.size();++i){
        Log( wxString::Format( wxT("\t") ) );
        wxString codeString(codeBuffer[i].c_str(), wxConvUTF8);
        Log( wxString::Format( codeString ) );
        Log( wxString::Format( wxT("\n") ) );
    }
    Log( wxString::Format( wxT("end") ) );
//        cout << i << ": " << codeBuffer[i] << endl;

//    Log( wxString::Format( wxT("   ") ) );
//    for (size_t i = 0; i < nShapes.size(); ++i)
//        Log( wxString::Format( wxT("%d "), i ) );
//    Log( wxString::Format( wxT("\n") ) );
//    for (size_t i = 0; i < nShapes.size(); ++i){
//        Log( wxString::Format( wxT("%d "), i ) );
//        for (size_t j = 0; j < nShapes.size(); ++j)
//            Log( wxString::Format( wxT("%d "), adjMatrix[i][j] ) );
//        Log( wxString::Format( wxT("\n") ) );
//    }
}

void GUI_MainFrm::CompileTool( wxCommandEvent& event )
{
    wxMessageBox( wxT("Compilación terminada con exito!") );
}

void GUI_MainFrm::ProgramTool( wxCommandEvent& event )
{
    wxMessageBox( wxT("Programación realizada!") );
}

void GUI_MainFrm::CleanUp()
{
    m_DiagramManager.SetShapeCanvas(NULL);
	m_DiagramManager.Clear();
 	Destroy();
}

void GUI_MainFrm::CleanLog()
{
    GUI_MainFrm *m_pParentFrame = (GUI_MainFrm*)wxGetApp().GetTopWindow();
    m_pParentFrame->m_TextEditor->Clear();
}

void GUI_MainFrm::Log(const wxString& msg)
{
    GUI_MainFrm *m_pParentFrame = (GUI_MainFrm*)wxGetApp().GetTopWindow();
    m_pParentFrame->m_TextEditor->AppendText( msg );
}
